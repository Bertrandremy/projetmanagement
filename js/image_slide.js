$(document).ready(function() {    
  
  // exécuter le diaporama , réglez 4 secondes ( 4000) pour chaque image
  slideShow(4000);

});

function slideShow(speed) {

  // ajoutez un ' li ' élément à la liste ' ul ' pour afficher la légende
  $('ul.slideshow').append('<li id="slideshow-caption" class="caption"><div class="slideshow-caption-container"><p></p></div></li>');

  //réglez l'opacité de toutes les images à 0
  $('ul.slideshow li').css({opacity: 0.0});
  
  // obtenir la première image et l'afficher
  $('ul.slideshow li:first').css({opacity: 1.0}).addClass('show');
  
  //obtenir la légende de la première image de l' attribut ' rel' et l'afficher
  $('#slideshow-caption p').html($('ul.slideshow li.show').find('img').attr('alt'));
    
  // afficher la légende
  $('#slideshow-caption').css({opacity: 0.6, bottom:0});
  
  // appeler la fonction de la galerie pour exécuter le diaporama 
  var timer = setInterval('gallery()',speed);
  
  //pause le diaporama sur la souris sur
  $('ul.slideshow').hover(
    function () {
      clearInterval(timer); 
    },  
    function () {
      timer = setInterval('gallery()',speed);     
    }
  );  
}

function gallery() {

  //Si aucune image ont la classe de spectacle, saisir la première image
  var current = ($('ul.slideshow li.show')?  $('ul.slideshow li.show') : $('#ul.slideshow li:first'));

  // en essayant d'éviter question de vitesse
  if(current.queue('fx').length == 0) {

    // obtenir l'image suivante , si elle atteint la fin du diaporama , faire pivoter retour à la première image
    var next = ((current.next().length) ? ((current.next().attr('id') == 'slideshow-caption')? $('ul.slideshow li:first') :current.next()) : $('ul.slideshow li:first'));
      
    // obtenir l'image suivante légende
    var desc = next.find('img').attr('alt');  
  
    // définir le fondu en vigueur pour la prochaine image , montrer classe a plus z-index
    next.css({opacity: 0.0}).addClass('show').animate({opacity: 1.0}, 1000);
    
    // masquer la légende d'abord, puis définir et d'afficher la légende
    $('#slideshow-caption').slideToggle(300, function () { 
      $('#slideshow-caption p').html(desc); 
      $('#slideshow-caption').slideToggle(500); 
    });   
  
    // masquer l'image actuelle
    current.animate({opacity: 0.0}, 1000).removeClass('show');

  }
}